<?php
/**
 * Created by PhpStorm.
 * User: vini-note
 * Date: 19/10/15
 * Time: 02:50
 * @todo Extrair categoria do anime
 * @todo Extrair key do anime
 * @todo Extrair número do episódio do anime
 */

include "vendor/autoload.php";

class MyCrawler extends PHPCrawler
{
    function handleDocumentInfo(PHPCrawlerDocumentInfo $PageInfo)
    {
        if($PageInfo->received) {
            echo $PageInfo->url."\n";
            if($PageInfo->content_type == 'application/x-javascript') {
                $data = $this->extractData($PageInfo->content);
                var_dump($data);
            }
        }

    }

    public function findVideos($url)
    {
        if(preg_match('/.mp4/', $url)) {
            return $url;
        }
    }

    public function findTitle($url)
    {
        if(preg_match('/video/', $url)) {
            return $url;
        }
    }

    public function extractData($content)
    {
        $data = array();
        // Encontra vídeos no conteúdo do arquivo
        preg_match_all('#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', $content, $match);
        $video_urls = array_filter($match[0], array($this, 'findVideos'));

        $videos = $this->detectVideoQuality($video_urls);
        $data['video'] = $videos;

        // Extrai as urls que contém o título do vídeo
        $title_urls = array_filter($match[0], array($this, 'findTitle'));
        $data['title'] = str_replace('-', ' ', end(explode('/', array_shift($title_urls))));

        return $data;
    }

    public function detectVideoQuality($videos)
    {
        foreach($videos as $video) {
            $v = end(explode('/', $video));
            if(preg_match('/_hd./', $v))
                $vid['hd'] = $video;
            else
                $vid['sd'] = $video;
        }
        return $vid;
    }
}

$crawler = new MyCrawler();
$crawler->setURL("http://www.anitube.se/video/86029/Dragon-Ball-Super-16");
$crawler->setLinkExtractionTags(array("href", "src"));
$crawler->addURLFilterRule("#\.(jpg|css|jpeg|gif|png|js)$# i");
$crawler->addContentTypeReceiveRule("#text/html#");
$crawler->addContentTypeReceiveRule("#application/x-javascript#");
$crawler->addLinkPriority("/config/", 2);
$crawler->addLinkPriority("/\/video\//", 1);
$crawler->setPageLimit(100);

$crawler->go();
