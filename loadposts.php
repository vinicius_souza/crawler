<?php
/**
 * Created by PhpStorm.
 * User: vini-note
 * Date: 26/10/15
 * Time: 19:34
 *
 * Script para carregar os posts a partir dos dados coletados pelo crawler.
 *
 *
 */
require 'vendor/autoload.php';

use MrRio\ShellWrap as sh;
use Cocur\Slugify\Slugify;

// Instancia a base de dados Mongodb
$client = new \Sokil\Mongo\Client('mongodb://127.0.0.1');
$client->useDatabase('animes');
$collection = $client->getCollection('episodios');

// Cria o cursor para iterar os documentos (episódios)
$cursor = $collection->find(['an_id', 'an_name', 'key']);
$result = $cursor->findAll();

$eps = array();
$categories = array();
$slugify = new Slugify();

// carrega os registros em memória para evitar erros do cursor
echo "Carregando dados do cursor para a memória...\n";
foreach($result as $documentId => $document) {
    $eps[] = array('id' => $document->get('an_id'),
        'title' => $document->get('an_name'),
        'key' => $document->get('key'),
        'cat_id' => $document->get('cat_id'),
        'cat_name' => $document->get('cat_name'));
}
echo "Dados do cursor carregados...\n";

foreach($eps as $ep) {
    $id = $ep['id'];
    $title = $ep['title'];
    $post_title = htmlentities($title);
    $key = $ep['key'];
    $category_id = $ep['cat_id'];
    $term = $ep['cat_name'];

    preg_match_all('/\d+/', $title, $matches);
    $episode = (int) end($matches[0]);

    try {
        // Se não existir categoria, cria o slug baseado no nome do episódio
        if (empty($term) && empty($category_id)) {
            $term = preg_replace('/\d+/', '', $title);
        }

        $term_slug = $slugify->slugify($term);

        // Se o term já foi criado antes, usa um term já criado
        if ($ar_key = array_search($term_slug, $categories)) {
            $wp_category_slug = $categories[$ar_key];
        } else {
            // senão, cria o termo
            $result = sh::wp('term create', "ani_animes '{$term}'");
            $wp_category_id = extract_id($result);
            $categories[$wp_category_id] = $term_slug;
            $wp_category_slug = $categories[$wp_category_id];
        }
    } catch (\MrRio\ShellWrapException $e) {
        echo "\e[31mFalha: {$e->getMessage()} (linha: {$e->getLine()}): \e[39mcategory_slug: {$wp_category_slug}; video_id: {$ep['id']}; nome: {$ep['title']}\n";
    }

    try {
        // Cria um post (requer o WP-CLI instalado)
        $result = sh::wp('post create', "--post_title='{$post_title}' --post_type=ani_episode --post_format='video' --post_status='publish'");

    } catch (\MrRio\ShellWrapException $e) {
        echo "\e[31mFalha: {$e->getMessage()} (linha: {$e->getLine()}): \e[39mpost_id: {$post_id}; video_id: {$ep['id']}; nome: {$ep['title']}\n";
    }

    $post_id = extract_id($result);

    try {
        // Cria o post-meta key no post (requer o WP-CLI instalado)
        sh::wp("post meta add {$post_id}", "key {$key}");
        sh::wp("post meta add {$post_id}", "video_id {$ep['id']}");
        sh::wp("post meta add {$post_id}", "episode {$episode}");

        // Cria um relacionamento entre o post e o term (taxonomia ani_animes)
        sh::wp("post term set {$post_id} ani_animes {$wp_category_slug}");

        // Cria a thumbnail do post (requer o WP-CLI instalado)
        /*sh::wp("media import http://static.anitube.se/thumb/1_{$id}.jpg",
            " --post_id={$post_id} --title='{$title}' --featured_image");*/

    } catch (\MrRio\ShellWrapException $e) {
        echo "\e[31mFalha: {$e->getMessage()} (linha: {$e->getLine()}): \e[39mpost_id: {$post_id}; video_id: {$ep['id']}; nome: {$ep['title']}\n";

    }

    echo "\e[32mSucesso: \e[39mpost_id: {$post_id}; video_id: {$ep['id']}; nome: {$ep['title']}\n";
}

// Extrai o id do objeto da mensagem retornada pelo WP-CLI
function extract_id($result)
{
    preg_match_all('/\d+/', $result->__toString(), $matches);
    $object_id = end($matches[0]);

    return intval($object_id);
}